% ExampleScript.m
%
% This script demonstrates use of the MadgwickAHRS and MahonyAHRS algorithm
% classes with example data. ExampleData.mat contains calibrated gyroscope,
% accelerometer and magnetometer data logged from an AHRS device (x-IMU)
% while it was sequentially rotated from 0 degrees, to +90 degree and then
% to -90 degrees around the X, Y and Z axis.  The script first plots the
% example sensor data, then processes the data through the algorithm and
% plots the output as Euler angles.
%
% Note that the Euler angle plot shows erratic behaviour in phi and psi
% when theta approaches �90 degrees. This due to a singularity in the Euler
% angle sequence known as 'Gimbal lock'.  This issue does not exist for a
% quaternion or rotation matrix representation.
%
% Date          Author          Notes
% 28/09/2011    SOH Madgwick    Initial release
% 13/04/2012    SOH Madgwick    deg2rad function no longer used
% 06/11/2012    Seb Madgwick    radian to degrees calculation corrected

%% Start of script

addpath('quaternion_library');      % include quaternion library
close all;                          % close all figures
% clear;                              % clear all variables
% clc;                                % clear the command terminal

%% Import and plot sensor data

% load('ExampleData.mat');
data = table2array(Big22);
% 
% Accelerometer = data(:,4:6);
% gravity = mean(Accelerometer(1:500,1));
% 
% data = data(500:length(data),:);
gravity = 0;
data(:,4)=data(:,4)-mean(data(1:100,4));
data(:,5)=data(:,5)-mean(data(1:100,5));
data(:,6)=data(:,6)-mean(data(1:100,6));

Gyroscope = data(:,1:3);
Accelerometer = data(:,4:6);
Magnetometer = data(:,7:9);
time = linspace(0,length(Accelerometer), length(Accelerometer));


figure('Name', 'Sensor Data');
axis(1) = subplot(3,1,1);
hold on;
plot(time, Gyroscope(:,1), 'r');
plot(time, Gyroscope(:,2), 'g');
plot(time, Gyroscope(:,3), 'b');
legend('X', 'Y', 'Z');
xlabel('Time (s)');
ylabel('Angular rate (deg/s)');
title('Gyroscope');
hold off;
axis(2) = subplot(3,1,2);
hold on;
plot(time, Accelerometer(:,1), 'r');
plot(time, Accelerometer(:,2), 'g');
plot(time, Accelerometer(:,3), 'b');
legend('X', 'Y', 'Z');
xlabel('Time (s)');
ylabel('Acceleration (g)');
title('Accelerometer');
hold off;
axis(3) = subplot(3,1,3);
hold on;
plot(time, Magnetometer(:,1), 'r');
plot(time, Magnetometer(:,2), 'g');
plot(time, Magnetometer(:,3), 'b');
legend('X', 'Y', 'Z');
xlabel('Time (s)');
ylabel('Flux (G)');
title('Magnetometer');
hold off;
linkaxes(axis, 'x');


%% filters

% d = designfilt('bandpassiir','FilterOrder',6, ...
%     'HalfPowerFrequency1',1,'HalfPowerFrequency2',5, ...
%     'SampleRate',250);


% Accelerometer = filtfilt(d,Accelerometer);

[b,a] = butter(6,[0.1 0.5],'stop');
Accelerometer = filter(b,a,Accelerometer);
%% Split data in armswing

normAcc = Accelerometer(:,1) - mean(Accelerometer(1:100,1));
[pks,locs] = findpeaks(normAcc);
pks_locs = [pks,locs];
pks_locs = pks_locs(pks_locs(:,1)>0.85*max(pks_locs(:,1)),:);

pks_locs_refined=zeros(0,2);
n = 1;
for m=1:length(pks_locs)-1
    if pks_locs(m+1,2)-pks_locs(m,2)>50
        pks_locs_refined(n,:)=pks_locs(m,:);
        n=n+1;
    end
end


peak_locations = pks_locs_refined(:,2);


figure()
hold on
plot(pks_locs_refined(:,2), pks_locs_refined(:,1), 'ro')
plot(linspace(0,length(normAcc),length(normAcc)),normAcc, 'b') 
hold off

full_data = data;
length_check=zeros(length(peak_locations)-1,1);
all_displacement = zeros(length(data),3);
euclidian_distances = zeros(length(peak_locations),1);

figure('Name', 'Orientated Displacement');
zlabel({'z axis'});
ylabel({'y axis'});
xlabel({'x axis'});
title({'3D compound displacement'});
hold on

for i=2:length(peak_locations)-2
    data_segment=full_data(peak_locations(i):peak_locations(i+1),:);
    length_check(i) = length(data_segment);
    Gyroscope = data_segment(:,1:3);
    Accelerometer = data_segment(:,4:6);
    Magnetometer = data_segment(:,7:9);
    time = linspace(0,length(Accelerometer), length(Accelerometer));
    %% Process sensor data through algorithm

    AHRS = MadgwickAHRS('SamplePeriod', 1/250, 'Beta', 0.1);
    %AHRS = MahonyAHRS('SamplePeriod', 1/256, 'Kp', 0.5);

    quaternion = zeros(length(time), 4);
    for t = 1:length(time)
        AHRS.Update(Gyroscope(t,:) * (pi/180), Accelerometer(t,:), Magnetometer(t,:));	% gyroscope units must be radians
        quaternion(t, :) = AHRS.Quaternion;
    end

    %% Plot algorithm output as Euler angles
    % The first and third Euler angles in the sequence (phi and psi) become
    % unreliable when the middle angles of the sequence (theta) approaches �90
    % degrees. This problem commonly referred to as Gimbal Lock.
    % See: http://en.wikipedia.org/wiki/Gimbal_lock

    euler = quatern2euler(quaternConj(quaternion)) * (180/pi);	% use conjugate for sensor frame relative to Earth and convert to degrees.

%     figure('Name', 'Euler Angles');
%     hold on;
%     plot(time, euler(:,1), 'r');
%     plot(time, euler(:,2), 'g');
%     plot(time, euler(:,3), 'b');
%     title('Euler angles');
%     xlabel('Time (s)');
%     ylabel('Angle (deg)');
%     legend('\phi', '\theta', '\psi');
%     hold off;


    R = quatern2rotMat(quaternConj(quaternion));
    accOr = zeros(length(Accelerometer),3);

    for j=1:length(Accelerometer)
       Accelerometer(j,:) = Accelerometer(j,:);
       accOr(j,:) = Accelerometer(j,:)*inv(R(:,:,j));
    end

    vx = cumtrapz(accOr(:,1).*9.81, time);
    vy = cumtrapz(accOr(:,2).*9.81, time);
    vz = cumtrapz(accOr(:,3).*9.81, time);
    
    vx = filter(b,a,vx);
    vy = filter(b,a,vy);
    vz = filter(b,a,vz);
    
    dx = cumtrapz(vx, time);
    dy = cumtrapz(vy, time);
    dz = cumtrapz(vz, time);
    
    dx = filter(b,a,dx);
    dy = filter(b,a,dy);
    dz = filter(b,a,dz);

    euc = zeros(length(dx),1);
    for k=1:length(dx)-1
        euc(k) = sqrt((dx(k+1)-dx(k))^2+(dy(k+1)-dy(k))^2+(dz(k+1)-dz(k))^2);
    end
    
    euclidian_distances(i) = sum(euc);

    hold on
    plot3(dx,dy,dz,'o')
    all_displacement(peak_locations(i):peak_locations(i+1),:) = [dx;dy;dz]';
end
hold off
%% End of script